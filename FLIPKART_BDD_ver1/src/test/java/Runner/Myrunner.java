package Runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"F:\\java_selenium_practice\\FLIPKART_BDD_ver1\\src\\test\\resources\\Featurefile"},
		glue= {"ApplicationHooks","stepdefinition"},
		plugin = {"pretty"}
		)

public class Myrunner {

}
