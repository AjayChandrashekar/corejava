package stepdefinition;

import java.util.Properties;

import org.junit.Assert;

import driverfactory.Diverfactory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pages.welcomepage;
import utility.configreader;

public class WelcomeStepdef {

	configreader cf=new configreader();
	private Properties prop=cf.init_prop();
	welcomepage wp=new welcomepage(Diverfactory.get_driver());
	@Given("user is on welcome page")
	public void user_is_on_welcome_page() {
		Diverfactory.get_driver().get(prop.getProperty("url"));
	}

	@Then("user gets the title of the page and the title should be {string}")
	public void user_gets_the_title_of_the_page_and_the_title_should_be(String extitle) {
		String act_title=wp.get_title();
		Assert.assertEquals(extitle, act_title);
	}

	@Then("the logo should be displayed")
	public void the_logo_should_be_displayed() {
		boolean logo=wp.logo_display();
		Assert.assertTrue(logo);
	}
}
