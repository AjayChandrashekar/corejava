package ApplicationHooks;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import driverfactory.Diverfactory;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import utility.configreader;

public class Apphooks {

	private WebDriver driver;
	private Diverfactory df;
	private configreader cf;
	private Properties prop;
	
	@Before(order = 0)
	public void setup() {
		cf=new configreader();
		prop=cf.init_prop();
	}
	
	@Before(order = 1)
	public void launch_browser() {
		String browser=prop.getProperty("browser");
		df=new Diverfactory();
		driver=df.init_driver(browser);
	}
	
	@After(order = 0)
	public void teardown() {
		driver.close();
	}
	
	
}
