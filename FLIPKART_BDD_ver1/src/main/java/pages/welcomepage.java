package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class welcomepage {

	private WebDriver driver;
	
	private By logo=By.xpath("//img[@alt='Flipkart']");
	
	public welcomepage(WebDriver driver) {
		this.driver=driver;
	}
	
	public String get_title() {
		return driver.getTitle();
	}
	
	public boolean logo_display() {
		return driver.findElement(logo).isDisplayed();
	}
	
	
}
