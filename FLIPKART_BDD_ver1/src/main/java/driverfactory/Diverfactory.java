package driverfactory;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Diverfactory {

	public WebDriver driver;
	
	public static ThreadLocal<WebDriver> tl=new ThreadLocal<>();
	
	public WebDriver init_driver(String browser){
		
		if(browser.equalsIgnoreCase("chrome")) {
			WebDriverManager.chromedriver().setup();
			ChromeOptions co=new ChromeOptions();
			co.addArguments("--remote-allow-origins=*");
			tl.set(new ChromeDriver(co));
		}else if(browser.equalsIgnoreCase("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			tl.set(new FirefoxDriver());
		}else {
			System.out.println("invalid browser");
		}
		
		
		get_driver().manage().window().maximize();
		get_driver().manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
		get_driver().manage().timeouts().pageLoadTimeout(Duration.ofSeconds(20));
		get_driver().manage().deleteAllCookies();
		
		return get_driver();
	}
	
	
	public static synchronized WebDriver get_driver() {
		return tl.get();
	}
	
}
