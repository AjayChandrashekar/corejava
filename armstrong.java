public class armstrong{
	public static void main(String args[]){
		int num=371;				//local var num initialized to 371
		int temp=num;				//local var temp initialized to value of num
		int output=0,length=0;			//local var output and length initialized to 0
		
		while(temp!=0){				//while loop to find the length of temp
			temp=temp/10;			//perform normal division to remove last digit of temp
			length++;			//increment length by 1 in every iteration
		}

		temp=num;				//reinitialize temp to num
		while(temp!=0){				//while temp not equal to 0 compute sum of power 
			output=output+computepower(temp%10,length);		//innvoke computepower() with last digit of temp and length and perform sum with output var
			temp=temp/10;			//remove last digit of temp
		}
	
		if(num==output)				//compare num with output if true print arm strong number else print not arm strong number 
			System.out.println("The given number is arm strong number");  //print arm strong number
		else
			System.out.println("The given number is not an arm strong number"); //print not arm strong number

	
	}

	public static int computepower(int a,int l){
		int p=1; 
		for(int i=0;i<l;i++){	//iterate the loop to perform multiplication of a, l number of times
			p=p*a;
		}
		return p;      //return the power
	}
}


==============================================
Iteration 1
==============================================
num=371
temp=371
output=0 length=0

while(371!=0){
	temp=371/10;----->temp=37
	length=1
}

while(37!=0){
	temp=37/10;----->temp=3
	length=2
}

while(3!=0){
	temp=3/10;----->temp=0
	length=3
}

temp=371

while(371!=0){

	output=0+computepower(371%10,3)------->computepower(1,3)	for(i=0;i<3;i++) 
										p=1*1=1
										
										1<3
										p=1*1=1
										2<3
										p=1*1=1
										3<3 for loop fails and return value of p
	output=0+1=1
	temp=378/10=37


while(37!=0){

	output=512+computepower(37%10,3)------->computepower(7,3)	for(i=0;i<3;i++) 
										p=1*7=7
										
										1<3
										p=7*7=49
										2<3
										p=49*7=343
										3<3 for loop fails and return value of p
	output=1+343=344
	temp=37/10=3

while(3!=0){

	output=344+computepower(3%10,3)------->computepower(3,3)	for(i=0;i<3;i++) 
										p=1*3=3
										
										1<3
										p=3*3=9
										2<3
										p=9*3=27
										3<3 for loop fails and return value of p
	output=344+27=371
	temp=3/10=0	

while(0!=0)---->false

if(num==output)
    371==371---->true

print the given num is arm strong number